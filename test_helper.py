from html_count.src import helpers

import pytest

def test_token():
    text = "Tell the audience what you're going to say. Say it. Then tell them what you've said."
    feq_dict = helpers.token_count(text)
    assert feq_dict == {"tell": 2,
        "the": 1,
        "audience": 1,
        "what": 2,
        "you're": 1,
        "going": 1,
        "to": 1,
        "say": 2,
        "it": 1,
        "then": 1,
        "them": 1,
        "you've": 1,
        "said": 1}

def test_parser():
    parser = helpers.HTMLParser()
    pass

def test_checkMemory():
    assert helpers.checkMemory(500)