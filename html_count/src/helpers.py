import psutil
import urllib.request
import nltk

from typing import Dict, List, Union
from html.parser import HTMLParser

nltk.download('punkt')

class TagParser(HTMLParser):

    def __init__(self, *, convert_charrefs: bool = ...) -> None:
        super().__init__(convert_charrefs=convert_charrefs)
        self.default()

    def default(self)->None:
        self.__text_list = list()
        self.__exclude_tag = set(["html", "script", "style", "noscript"])
        self.__store_flag = False
        self.tag_set = set()

    def reset(self) -> None:
        super().reset()
        self.default()


    def setExcludedTags(self, list: List[str]):
        self.__exclude_tag = list

    def handle_starttag(self, tag, attrs):
        if tag not in self.__exclude_tag:
            self.tag_set.add(tag)
            self.__store_flag = True


    def handle_data(self, data):
        if self.__store_flag:
            # print("Data     :", data)
            self.__text_list.append(data)
            self.__store_flag = False

    def getTaxList(self)->List[str]:
        return self.__text_list

def getHTML(url: str)->str:
    """Get HTML form url

    Args:
        url (str): url

    Returns:
        html(str): html string

    Raises: urllib.error.URLError
    """

    with urllib.request.urlopen(url) as fp:
        byte_data = fp.read()
        html = byte_data.decode("utf8")
    return html

def token_count(text: Union[str, List[str]])->Dict[str, int]:
    # tokenizer = nltk.RegexpTokenizer(r"\w+?'+\w+|\w+")
    tokenizer = nltk.tokenize.TweetTokenizer()

    tokens = list()
    if isinstance(text,str):
        tokens = tokenizer.tokenize(text.lower())
    else:
        for t in text:
            tokens = tokens + tokenizer.tokenize(t.lower()) # very inefficient, optimize later

    result = nltk.FreqDist(tokens)
    result.pop(".")
    return dict(result)

def checkMemory(limit: float)->bool:
    """Check memory usage is below limit

    Args:
        limit (float): maximum percentage of memory use

    Returns:
        bool: True if memory usage is below limit.
    """
    
    return psutil.virtual_memory().percent < limit

def checkDependantConnection()->bool:
    return True