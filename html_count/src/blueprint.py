from flask import Blueprint, Response, jsonify, request
from .helpers import getHTML, token_count, TagParser, checkMemory, checkDependantConnection

html_count = Blueprint("html_count", __name__)

@html_count.route("/count")
def colors()->Response:
  """Get the word frequency of the HTML from the URL.
  ---
  parameters:
    - name: url
      in: query
      type: string
      required: true
  definitions:
    FreqDist:
      type: object

  responses:
    200:
      description: Dict of words to its number of occurrences in the HTML
      schema:
        $ref: '#/definitions/FreqDist'
      examples:
        {"cats": 9000, "dogs": 1}
  """
  if "url" not in request.args:
    return "Invalid Parameters", 400

  url = request.args.get("url")
  print(f"url: {url}")
  try:
      html_str = getHTML(url)
  except:
      return "Invalid URL", 400

  parser = TagParser()
  parser.feed(html_str)
  tokens = parser.getTaxList()
  print(parser.tag_set)
  parser.close()
  
  result = token_count(tokens)
  return jsonify(result)

@html_count.route("/health")
def health()->Response:
  """Check server health.
  ---
  responses:
    200:
      description: healthy
    400:
      description: unhealthy
  """
  health = checkMemory(90) # arbitrary threshold of 90% memory use
  health &= checkDependantConnection()

  if health:
    return "healthy", 200
  else:
    return "unhealthy", 400