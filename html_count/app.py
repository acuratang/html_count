from venv import create
from src.blueprint import html_count

from flask import Blueprint, Flask
from flasgger import Swagger

app = Flask(__name__)

app.register_blueprint(html_count)
swagger = Swagger(app)

if __name__ == "__main__":
  app.run(host="0.0.0.0", debug=True)
