# syntax=docker/dockerfile:1
FROM python:3.10

RUN apt-get update && apt-get upgrade -y && apt-get clean

COPY html_count requirements.txt ./
RUN pip install -r requirements.txt
EXPOSE 5000

# CMD [ "python3", "app.py"]
CMD ["gunicorn", "-w", "2", "--threads", "2", "--bind", "0.0.0.0:5000", "app:app"]