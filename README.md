# Tech Stack
Flask was chosen as the Server because it is light weight, have a nice API and is widely used.

Swagger is commonly used by Node devs and it is a good standard way to do API documentation, it is also handy for manual testing.

flasgger was chosen because it seems to be the best choice to have Swagger with Flask.

pytest was choses only because I am use to it and it seems to do the job.

nltk is a very powerful NLP lib which comes with a lot fo tokenization options which makes switching between them very easy.

Docker is the only realistic choice for containerization.

Gunicorn to scale the processes, currently using 2 workers, 2 threads, the ratio between worker and threads are not optimized 

The average latency is about 220ms, optimization is needed...
In terms of  of performance there are lot of string operations and because string in python is immutable there are a lot of copies, switching to a mutable string type would be preferred. Also the dev Flask server will be very slow

Ran out of time so the tests are very light, no https, and I did not add nginx as revers proxy... 
So it is still using the dev flask server

# run test
```bash
    html_count$ pip install -r requirements.txt
    html_count$ pytest -v
```

# build docker 
```bash
    html_count$ docker build -t html_count .
```

# run docker 
```bash
    docker run -it --rm -p 5000:5000 html_count
```

# Inspect API
http://localhost:5000/apidocs/

